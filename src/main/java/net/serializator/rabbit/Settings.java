package net.serializator.rabbit;

import net.serializator.rabbit.utils.LocationUtil;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Settings {
    private final Location spawn;
    private final List<Location> spawns;

    public Settings(Rabbit instance) {
        spawn = LocationUtil.deserialize(instance.getConfig().getString("spawn"));
        spawns = new ArrayList<Location>() {{
            instance.getConfig().getStringList("spawns").forEach(spawn -> add(LocationUtil.deserialize(spawn)));
        }};
    }

    public Location getSpawn() {
        return spawn.clone();
    }

    public List<Location> getSpawns() {
        return Collections.unmodifiableList(new ArrayList<Location>() {{
            spawns.forEach(spawn -> add(spawn.clone()));
        }});
    }
}
