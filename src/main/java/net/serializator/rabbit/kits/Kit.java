package net.serializator.rabbit.kits;

import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.List;

public class Kit {
    private final String name;
    private final int costs;
    private final List<ItemStack> contents;
    private final List<ItemStack> armor;

    public Kit(String name, int costs, List<ItemStack> contents, List<ItemStack> armor) {
        this.name = name;
        this.costs = costs;
        this.contents = Collections.unmodifiableList(contents);
        this.armor = Collections.unmodifiableList(armor);
    }

    public String getName() {
        return name;
    }

    public int getCosts() {
        return costs;
    }

    public List<ItemStack> getContents() {
        return contents;
    }

    public List<ItemStack> getArmor() {
        return armor;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }

        if((object == null) || (object.getClass() != getClass())) {
            return false;
        }

        return ((Kit) object).getName().equals(name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
