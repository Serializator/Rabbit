package net.serializator.rabbit.kits;

import net.serializator.rabbit.Rabbit;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class KitManager {
    private final Rabbit instance;
    private final Map<String, Kit> kits;

    public KitManager(Rabbit instance) {
        this.instance = instance;
        this.kits = new HashMap<>();

        load();
    }

    public Map<String, Kit> getKits() {
        return Collections.unmodifiableMap(kits);
    }

    public Kit getKit(String name) {
        return kits.get(name);
    }

    public Kit getKit(ItemStack stack) {
        /* TODO: Involve some reflection to access the NBT of the given ItemStack. */
        return null;
    }

    @SuppressWarnings("unchecked")
    private void load() {
        if(!instance.getConfig().isConfigurationSection("kits")) {
            return;
        }

        instance.getConfig().getConfigurationSection("kits").getKeys(false).forEach(name -> {
            int costs = instance.getConfig().getInt("kits." + name + ".costs");

            List<ItemStack> contents = new LinkedList<>(((List<ItemStack>) instance.getConfig().getList("kits." + name + ".contents")));
            List<ItemStack> armor = new LinkedList<>(((List<ItemStack>) instance.getConfig().getList("kits." + name + ".armor")));

            kits.put(name, new Kit(name, costs, contents, armor));
        });
    }

    public void add(String name, int costs, List<ItemStack> contents, List<ItemStack> armor) {
        kits.computeIfAbsent(name, key -> {
            Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
                instance.getConfig().set("kits." + name + ".costs", costs);
                instance.getConfig().set("kits." + name + ".contents", contents);
                instance.getConfig().set("kits." + name + ".armor", armor);
                instance.saveConfig();
            });

            return new Kit(name, costs, contents, armor);
        });
    }

    public void remove(String name) {
        kits.computeIfPresent(name, (key, value) -> {
            Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
                instance.getConfig().set("kits." + name, null);
                instance.saveConfig();
            });

            return null;
        });
    }
}
