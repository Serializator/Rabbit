package net.serializator.rabbit.storage;

import com.zaxxer.hikari.HikariDataSource;
import net.serializator.rabbit.Rabbit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL {
    private final Rabbit instance;
    private HikariDataSource source;

    public MySQL(Rabbit instance) throws SQLException {
        this.instance = instance;

        connect();
        createTables();
    }

    public synchronized void connect() {
        if((source != null) && !source.isClosed()) {
            return;
        }

        source = new HikariDataSource();
        source.setMaximumPoolSize(10);
        source.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        source.addDataSourceProperty("serverName", instance.getConfig().getString("mysql.address"));
        source.addDataSourceProperty("port", instance.getConfig().getInt("mysql.port"));
        source.addDataSourceProperty("databaseName", instance.getConfig().getString("mysql.database"));
        source.addDataSourceProperty("user", instance.getConfig().getString("mysql.username"));
        source.addDataSourceProperty("password", instance.getConfig().getString("mysql.password"));
    }

    private void createTables() throws SQLException {
        try(Connection connection = getConnection();
            Statement statement = connection.createStatement()) {

            statement.addBatch("CREATE TABLE IF NOT EXISTS `players` (" +
                    "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
                    "`unique_id` CHAR(32) NOT NULL," +
                    "`cached_name` VARCHAR(16) NOT NULL," +
                    "`coins` INT UNSIGNED NOT NULL DEFAULT 0," +
                    "" +
                    "CONSTRAINT PRIMARY KEY (`id`)," +
                    "CONSTRAINT UNIQUE (`unique_id`));");

            statement.addBatch("CREATE TABLE IF NOT EXISTS `player_kits` (" +
                    "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
                    "`player_id` INT UNSIGNED NOT NULL," +
                    "`kit` VARCHAR(255) NOT NULL," +
                    "" +
                    "CONSTRAINT PRIMARY KEY (`id`)," +
                    "CONSTRAINT UNIQUE (`player_id`, `kit`)," +
                    "CONSTRAINT FOREIGN KEY (`player_id`) REFERENCES `players` (`id`));");

            statement.executeBatch();
        }
    }

    public synchronized void disconnect() {
        if(source == null) {
            return;
        }

        source.close();
        source = null;
    }

    public PreparedStatement prepare(Connection connection, String query, Object... parameters) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        for(int index = 0; index < parameters.length; index++) {
            statement.setObject(index, parameters[index]);
        }

        return statement;
    }

    public PreparedStatement prepare(String query, Object... parameters) throws SQLException {
        return prepare(getConnection(), query, parameters);
    }

    public Connection getConnection() throws SQLException {
        return source.getConnection();
    }
}
