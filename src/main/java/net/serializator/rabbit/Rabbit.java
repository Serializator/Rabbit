package net.serializator.rabbit;

import net.serializator.rabbit.kits.KitManager;
import net.serializator.rabbit.listeners.PlayerListener;
import net.serializator.rabbit.listeners.WorldListener;
import net.serializator.rabbit.sessions.SessionManager;
import net.serializator.rabbit.states.StateManager;
import net.serializator.rabbit.storage.MySQL;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.logging.Level;

public class Rabbit extends JavaPlugin {
    private MySQL mysql;
    private Settings settings;
    private StateManager stateManager;
    private KitManager kitManager;
    private SessionManager sessionManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        try {
            mysql = new MySQL(this);
        } catch(SQLException exception) {
            getLogger().log(Level.WARNING, "Something went wrong with either the instantiation of a connection to the database or the creation of the necessary tables.", exception);
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        settings = new Settings(this);
        stateManager = new StateManager(this);
        kitManager = new KitManager(this);
        sessionManager = new SessionManager(this);

        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        getServer().getPluginManager().registerEvents(new WorldListener(this), this);
    }

    @Override
    public void onDisable() {
        mysql.disconnect();
    }

    public MySQL getMySQL() {
        return mysql;
    }

    public Settings getSettings() {
        return settings;
    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public KitManager getKitManager() {
        return kitManager;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }
}
