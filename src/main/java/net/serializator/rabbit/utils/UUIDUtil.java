package net.serializator.rabbit.utils;

import java.math.BigInteger;
import java.util.UUID;

public class UUIDUtil {
    private UUIDUtil() {}

    public static UUID getWithDashes(String uniqueId) {
        long mostSigBits = new BigInteger(uniqueId.substring(0, 16), 16).longValue();
        long leastSigBits = new BigInteger(uniqueId.substring(16, 32), 16).longValue();

        return new UUID(mostSigBits, leastSigBits);
    }

    public static String getWithoutDashes(UUID uniqueId) {
        return uniqueId.toString().replace("-", "");
    }
}
