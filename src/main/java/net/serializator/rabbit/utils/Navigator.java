package net.serializator.rabbit.utils;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.utils.builders.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Navigator {
    private static final int CONTENT_PER_PAGE = 7;
    private static final ItemStack statisticsStack = new ItemStackBuilder(Material.BOOK).setName(ChatColor.GREEN + "Statistics").getItemStack();
    private static final ItemStack nextStack = new ItemStackBuilder(Material.ARROW).setName(ChatColor.GREEN + "Next >").getItemStack();
    private static final ItemStack backStack = new ItemStackBuilder(Material.ARROW).setName(ChatColor.GREEN + "< Back").getItemStack();

    private final Rabbit instance;
    private final String title;
    private final Map<UUID, Viewer> viewers;
    private final List<ItemStack> contents;
    private final int pages;

    public Navigator(Rabbit instance, String title, List<ItemStack> contents) {
        this.instance = instance;
        this.title = title;
        this.viewers = new HashMap<>();
        this.contents = new LinkedList<>(contents);
        this.pages = ((int) Math.ceil(((double) contents.size()) / CONTENT_PER_PAGE));
    }

    public void open(Player player) {
        if(viewers.containsKey(player.getUniqueId())) {
            return;
        }

        viewers.put(player.getUniqueId(), new Viewer(player));
    }

    public Viewer get(Player player) {
        return viewers.get(player.getUniqueId());
    }

    private class Viewer {
        private final Player player;
        private final Inventory inventory;
        private int page;

        public Viewer(Player player) {
            this.player = player;
            this.inventory = Bukkit.createInventory(null, 27, title);
            this.page = 0;
        }

        private void calculateContents() {
            inventory.clear();

            for(int i = (CONTENT_PER_PAGE * page); ((i < contents.size()) && (i < ((CONTENT_PER_PAGE * page) + CONTENT_PER_PAGE))); i++ ) {
                inventory.setItem((10 + (i - (CONTENT_PER_PAGE * page))), contents.get(i));
            }

            if(page < (pages - 1)) {
                inventory.setItem(23, nextStack.clone());
            }

            inventory.setItem(22, statisticsStack.clone());

            if(page > 0) {
                inventory.setItem(21, backStack.clone());
            }
        }

        public void turn(int page) {
            if((page > 0) && (page < pages)) {
                this.page = page;
                calculateContents();
            }
        }

        public void forward() {
            if((page + 1) < pages) {
                turn(page + 1);
            }
        }

        public void backward() {
            if(page > 0) {
                turn(page - 1);
            }
        }

        @Override
        public boolean equals(Object object) {
            if(object == this) {
                return true;
            }

            if(!(object instanceof Viewer)) {
                return false;
            }

            return player.equals(((Viewer) object).player);
        }

        @Override
        public int hashCode() {
            return player.hashCode();
        }
    }
}
