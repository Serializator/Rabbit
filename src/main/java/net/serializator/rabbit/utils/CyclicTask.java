package net.serializator.rabbit.utils;

import net.serializator.rabbit.Rabbit;
import org.bukkit.scheduler.BukkitTask;

public abstract class CyclicTask implements Runnable {
    protected final Rabbit instance;
    private volatile BukkitTask task;

    public CyclicTask(Rabbit instance) {
        this.instance = instance;
    }

    public final synchronized void runTask() {
        cancel();
        instance.getServer().getScheduler().runTask(instance, this);
    }

    public final synchronized void runTaskAsynchronously() {
        cancel();
        instance.getServer().getScheduler().runTaskAsynchronously(instance, this);
    }

    public final synchronized void runTaskLater(long interval) {
        cancel();
        instance.getServer().getScheduler().runTaskLater(instance, this, interval);
    }

    public final synchronized void runTaskLaterAsynchronously(long interval) {
        cancel();
        instance.getServer().getScheduler().runTaskLaterAsynchronously(instance, this, interval);
    }

    public final synchronized void runTaskTimer(long delay, long interval) {
        cancel();
        instance.getServer().getScheduler().runTaskTimer(instance, this, delay, interval);
    }

    public final synchronized void runTaskTimerAsynchronously(long delay, long interval) {
        cancel();
        instance.getServer().getScheduler().runTaskTimerAsynchronously(instance, this, delay, interval);
    }

    public final void cancel() {
        if(task == null) {
            return;
        }

        task.cancel();
        task = null;
    }
}
