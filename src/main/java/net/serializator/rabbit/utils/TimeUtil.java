package net.serializator.rabbit.utils;

import java.util.concurrent.TimeUnit;

public class TimeUtil {
    private TimeUtil() {}

    public static String format(long duration, TimeUnit unit) {
        if(unit != TimeUnit.SECONDS) {
            duration = unit.toSeconds(duration);
        }

        long minutes = (duration / 60);
        long seconds = (duration % 60);

        return (minutes + ":" + ((seconds < 10) ? " " : "") + seconds);
    }
}
