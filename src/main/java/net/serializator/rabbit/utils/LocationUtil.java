package net.serializator.rabbit.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationUtil {
    private LocationUtil() {}

    public static String serialize(Location location, boolean abstractCoordinates, boolean serializeRotation) {
        StringBuilder result = new StringBuilder(location.getWorld().getName());

        result.append(":").append((abstractCoordinates ? location.getBlockX() : location.getX()));
        result.append(":").append((abstractCoordinates ? location.getBlockY() : location.getY()));
        result.append(":").append((abstractCoordinates ? location.getBlockZ() : location.getZ()));

        if(serializeRotation) {
            result.append(":").append(location.getYaw());
            result.append(":").append(location.getPitch());
        }

        return result.toString();
    }

    public static Location deserialize(String string) {
        String[] elements = string.split(":");
        World world = Bukkit.getWorld(elements[0]);

        double x = Double.parseDouble(elements[1]);
        double y = Double.parseDouble(elements[2]);
        double z = Double.parseDouble(elements[3]);

        float yaw = 0;
        float pitch = 0;

        if(elements.length >= 6) {
            yaw = Float.parseFloat(elements[4]);
            pitch = Float.parseFloat(elements[5]);
        }

        return new Location(world, x, y, z, yaw, pitch);
    }
}
