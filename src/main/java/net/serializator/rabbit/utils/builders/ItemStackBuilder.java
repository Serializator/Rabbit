package net.serializator.rabbit.utils.builders;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ItemStackBuilder {
    private final ItemStack stack;
    private final ItemMeta meta;

    public ItemStackBuilder(ItemStack stack) {
        this.stack = stack;
        this.meta = stack.getItemMeta();
    }

    public ItemStackBuilder(Material type) {
        this(new ItemStack(type));
    }

    public ItemStackBuilder(Material type, int amount) {
        this(new ItemStack(type, amount));
    }

    public ItemStackBuilder(Material type, int amount, short data) {
        this(new ItemStack(type, amount, data));
    }

    public ItemStackBuilder setName(String name) {
        meta.setDisplayName(name);
        return this;
    }

    public ItemStackBuilder setLore(List<String> lore) {
        meta.setLore(lore);
        return this;
    }

    public ItemStackBuilder setLore(String... lore) {
        return setLore(Arrays.asList(lore));
    }

    public ItemStackBuilder addEnchantment(Enchantment enchantment, int level) {
        stack.addEnchantment(enchantment, level);
        return this;
    }

    public ItemStackBuilder addEnchantments(Map<Enchantment, Integer> enchantments) {
        stack.addEnchantments(enchantments);
        return this;
    }

    public ItemStackBuilder addUnsafeEnchantment(Enchantment enchantment, int level) {
        stack.addEnchantment(enchantment, level);
        return this;
    }

    public ItemStackBuilder addUnsafeEnchantments(Map<Enchantment, Integer> enchantments) {
        stack.addUnsafeEnchantments(enchantments);
        return this;
    }

    public ItemStackBuilder addItemFlags(ItemFlag... flags) {
        meta.addItemFlags(flags);
        return this;
    }

    public ItemStackBuilder setColor(Color color) {
        if(meta instanceof LeatherArmorMeta) {
            ((LeatherArmorMeta) meta).setColor(color);
        }

        return this;
    }

    public ItemStackBuilder setOwner(String owner) {
        if(meta instanceof SkullMeta) {
            ((SkullMeta) meta).setOwner(owner);
        }

        return this;
    }

    public ItemStackBuilder setBasePotionData(PotionData data) {
        if(meta instanceof PotionMeta) {
            ((PotionMeta) meta).setBasePotionData(data);
        }

        return this;
    }

    /**
     * Originally returns whether or not the meta of the item changed as a result of the invocation of {@link PotionMeta#addCustomEffect(PotionEffect, boolean)}.
     */
    public ItemStackBuilder addCustomEffect(PotionEffect effect, boolean overwrite) {
        if(meta instanceof PotionMeta) {
            ((PotionMeta) meta).addCustomEffect(effect, overwrite);
        }

        return this;
    }

    /**
     * Originally returns whether or not the meta of the item changed as a result of the invocation of {@link PotionMeta#addCustomEffect(PotionEffect, boolean)}.
     */
    public ItemStackBuilder removeCustomEffect(PotionEffectType type) {
        if(meta instanceof PotionMeta) {
            ((PotionMeta) meta).removeCustomEffect(type);
        }

        return this;
    }

    public ItemStackBuilder setScaling(boolean scaling) {
        if(meta instanceof MapMeta) {
            ((MapMeta) meta).setScaling(scaling);
        }

        return this;
    }

    public ItemStack getItemStack() {
        stack.setItemMeta(meta);
        return stack;
    }
}
