package net.serializator.rabbit.states;

import net.serializator.rabbit.Rabbit;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

public class State {
    protected final Rabbit instance;
    private final Type type;
    protected final Class<? extends State> next;

    public State(Rabbit instance, Type type, Class<? extends State> next) {
        this.instance = instance;
        this.type = type;
        this.next = next;
    }

    public void start() {}

    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {}
    public void onPlayerLogin(PlayerLoginEvent event) {}
    public void onPlayerJoin(PlayerJoinEvent event) {}
    public void onPlayerQuit(PlayerQuitEvent event) {}
    public void onPlayerInteract(PlayerInteractEvent event) {}

    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    public void end() {}

    public final Type getType() {
        return type;
    }

    public final Class<? extends State> getNext() {
        return next;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }

        if(!(object instanceof State)) {
            return false;
        }

        return (((State) object).getType() == type);
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    public enum Type {
        LOBBY,
        GAME,
        TERMINATION
    }
}
