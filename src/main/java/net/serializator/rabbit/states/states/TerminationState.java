package net.serializator.rabbit.states.states;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.states.State;
import org.bukkit.event.player.PlayerLoginEvent;

public class TerminationState extends State {
    public TerminationState(Rabbit instance) {
        super(instance, Type.TERMINATION, null);
    }

    @Override
    public void onPlayerLogin(PlayerLoginEvent event) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "This server is currently terminating the match it was running.");
    }
}
