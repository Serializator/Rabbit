package net.serializator.rabbit.states.states.lobby;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.factories.ItemStackFactory;
import net.serializator.rabbit.states.State;
import net.serializator.rabbit.states.states.game.GameState;
import net.serializator.rabbit.utils.CyclicTask;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LobbyState extends State {
    private final CyclicTask task;

    public LobbyState(Rabbit instance) {
        super(instance, Type.LOBBY, GameState.class);
        this.task = new LobbyTask(instance);
    }

    @Override
    public void start() {
        task.runTaskTimer(0L, 20L);
    }

    @Override
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        if(Bukkit.getOnlinePlayers().size() >= instance.getSettings().getSpawns().size()) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "The match that this server is running is currently full.");
            return;
        }
    }

    @Override
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        player.getInventory().clear();
        player.getInventory().setItem(0, ItemStackFactory.getKitSelector());

        event.setJoinMessage(ChatColor.GRAY + player.getName() + ChatColor.YELLOW + " joined the match. (" + ChatColor.AQUA + Bukkit.getOnlinePlayers().size() + "/" + instance.getSettings().getSpawns() + ChatColor.YELLOW + ")");
    }

    @Override
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(ChatColor.GRAY + event.getPlayer().getName() + ChatColor.YELLOW + " left the match. (" + ChatColor.AQUA + (Bukkit.getOnlinePlayers().size() - 1) + "/" + instance.getSettings().getSpawns() + ChatColor.YELLOW + ")");
    }

    @Override
    public void end() {
        task.cancel();
    }
}
