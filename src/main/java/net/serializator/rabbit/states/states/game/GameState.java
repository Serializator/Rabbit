package net.serializator.rabbit.states.states.game;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.states.State;
import net.serializator.rabbit.states.states.TerminationState;
import net.serializator.rabbit.utils.CyclicTask;
import org.bukkit.event.player.PlayerLoginEvent;

public class GameState extends State {
    private final CyclicTask task;

    public GameState(Rabbit instance) {
        super(instance, Type.GAME, TerminationState.class);
        this.task = new GameTask(instance);
    }

    @Override
    public void start() {
        task.runTaskTimer(20L, 20L);
    }

    @Override
    public void onPlayerLogin(PlayerLoginEvent event) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "This server is currently running an active match.");
    }

    @Override
    public void end() {
        task.cancel();
    }
}
