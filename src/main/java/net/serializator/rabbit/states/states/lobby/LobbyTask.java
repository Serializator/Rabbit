package net.serializator.rabbit.states.states.lobby;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.utils.CyclicTask;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class LobbyTask extends CyclicTask {
    private int time;

    public LobbyTask(Rabbit instance) {
        super(instance);
        this.time = 30;
    }

    @Override
    public void run() {
        if(time > 0) {
            if(time <= 3) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendTitle(ChatColor.YELLOW + "Starting in " + time, ""));
            } else if((time % 10) == 0) {
                Bukkit.broadcastMessage(ChatColor.YELLOW + "The match is starting in " + time + ((time == 1) ? " second." : " seconds."));
            }

            time--;
        } else {
            instance.getStateManager().next();
            cancel();
        }
    }
}
