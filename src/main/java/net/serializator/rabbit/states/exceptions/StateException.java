package net.serializator.rabbit.states.exceptions;

public class StateException extends RuntimeException {
    public StateException(String message) {
        super(message);
    }
}
