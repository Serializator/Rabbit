package net.serializator.rabbit.states;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.states.exceptions.StateException;
import net.serializator.rabbit.states.states.TerminationState;
import net.serializator.rabbit.states.states.game.GameState;
import net.serializator.rabbit.states.states.lobby.LobbyState;

import java.util.HashMap;
import java.util.Map;

public class StateManager {
    private final Map<Class<? extends State>, State> states;
    private State current;

    public StateManager(Rabbit instance) {
        this.states = new HashMap<Class<? extends State>, State>() {{
            put(LobbyState.class, new LobbyState(instance));
            put(GameState.class, new GameState(instance));
            put(TerminationState.class, new TerminationState(instance));
        }};

        this.current = states.get(LobbyState.class);
    }

    public void next() throws StateException {
        if(current != null) {
            if(current.getNext() == null) {
                throw new StateException(String.format("There is no subsequent state for %s.", current.getType().name()));
            }

            current.end();
            current = states.get(current.getNext());
        } else {
            current = states.get(LobbyState.class);
        }

        current.start();
    }

    public State getCurrent() {
        return current;
    }
}
