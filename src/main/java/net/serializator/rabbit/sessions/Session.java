package net.serializator.rabbit.sessions;

import net.serializator.rabbit.kits.Kit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Session {
    private final UUID uniqueId;
    private final String name;
    private final Set<Kit> kits;
    private int coins;

    public Session(UUID uniqueId, String name, Set<Kit> kits, int coins) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.kits = new HashSet<>(kits);
        this.coins = coins;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(uniqueId);
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public Set<Kit> getKits() {
        return Collections.unmodifiableSet(kits);
    }

    public void addKit(Kit kit) {
        kits.add(kit);
    }

    public void removeKit(Kit kit) {
        kits.remove(kit);
    }

    public boolean hasKit(Kit kit) {
        return kits.contains(kit);
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }

        if(!(object instanceof Session)) {
            return false;
        }

        return ((Session) object).getUniqueId().equals(uniqueId);
    }

    @Override
    public int hashCode() {
        return uniqueId.hashCode();
    }
}
