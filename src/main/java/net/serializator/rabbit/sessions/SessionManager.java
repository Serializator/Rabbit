package net.serializator.rabbit.sessions;

import net.serializator.rabbit.Rabbit;
import net.serializator.rabbit.kits.Kit;
import net.serializator.rabbit.utils.UUIDUtil;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.*;

public class SessionManager {
    private final Rabbit instance;
    private final Map<UUID, Session> sessions;

    public SessionManager(Rabbit instance) {
        this.instance = instance;
        this.sessions = new HashMap<>();
    }

    public Map<UUID, Session> getSessions() {
        return Collections.unmodifiableMap(sessions);
    }

    public Session getSession(Player player) {
        return sessions.get(player.getUniqueId());
    }

    public boolean register(UUID uniqueId, String name) {
        String query = "SELECT `players`.`coins`, `kits`.`kit` FROM `players`" +
                "LEFT JOIN `player_kits` `kits` ON `players`.`id` = `kits`.`player_id`" +
                "WHERE `players`.`unique_id` = ?";

        return sessions.computeIfAbsent(uniqueId, key -> {
            try(Connection connection = instance.getMySQL().getConnection();
                ResultSet result = instance.getMySQL().prepare(connection, query, UUIDUtil.getWithoutDashes(uniqueId)).executeQuery()) {

                if(result.next()) {
                    int coins = result.getInt("coins");
                    Set<Kit> kits = new HashSet<>();

                    do {
                        Kit kit = instance.getKitManager().getKit(result.getString("kit"));

                        if(kit != null) {
                            kits.add(kit);
                        }
                    } while(result.next());

                    return new Session(uniqueId, name, kits, coins);
                } else {
                    return new Session(uniqueId, name, new HashSet<>(), 0);
                }
            } catch(SQLException exception) {
                exception.printStackTrace();
            }

            return null;
        }) != null;
    }

    public void unregister(UUID uniqueId) {
        sessions.computeIfPresent(uniqueId, (key, session) -> {
            try(Connection connection = instance.getMySQL().getConnection();
                PreparedStatement players = connection.prepareStatement("INSERT INTO `players` (`unique_id`, `cached_name`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `cached_name` = ?, `coins` = ?");
                PreparedStatement kits = connection.prepareStatement("INSERT IGNORE INTO `player_kits` (`player_id`, `kit`) VALUES ((SELECT `id` FROM `players` WHERE `unique_id` = ?), ?)")) {

                players.setString(1, UUIDUtil.getWithoutDashes(uniqueId));
                players.setString(2, session.getName());
                players.setString(3, session.getName());
                players.setInt(4, session.getCoins());

                for(Kit kit : session.getKits()) {
                    kits.setString(1, UUIDUtil.getWithoutDashes(uniqueId));
                    kits.setString(2, kit.getName());
                    kits.addBatch();
                }

                players.executeUpdate();
                kits.executeBatch();
            } catch(SQLException exception) {
                exception.printStackTrace();
            }

            return null;
        });
    }
}