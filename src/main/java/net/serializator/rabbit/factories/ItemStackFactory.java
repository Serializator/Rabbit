package net.serializator.rabbit.factories;

import net.serializator.rabbit.utils.builders.ItemStackBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemStackFactory {
    private static final ItemStack kitSelector = new ItemStackBuilder(Material.FEATHER).setName(ChatColor.YELLOW + "Select a Kit").getItemStack();

    private ItemStackFactory() {}

    public static ItemStack getKitSelector() {
        return kitSelector.clone();
    }
}
