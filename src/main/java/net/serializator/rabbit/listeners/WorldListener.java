package net.serializator.rabbit.listeners;

import net.serializator.rabbit.Rabbit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class WorldListener implements Listener {
    private final Rabbit instance;

    public WorldListener(Rabbit instance) {
        this.instance = instance;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        instance.getStateManager().getCurrent().onBlockPlace(event);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        instance.getStateManager().getCurrent().onBlockBreak(event);
    }
}
