package net.serializator.rabbit.listeners;

import net.serializator.rabbit.Rabbit;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

public class PlayerListener implements Listener {
    private final Rabbit instance;

    public PlayerListener(Rabbit instance) {
        this.instance = instance;
    }

    @EventHandler
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        instance.getStateManager().getCurrent().onAsyncPlayerPreLogin(event);

        if(event.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED) {
            if(!instance.getSessionManager().register(event.getUniqueId(), event.getName())) {
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "We were unable to fetch your profile.");
            }
        }
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        instance.getStateManager().getCurrent().onPlayerLogin(event);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        instance.getStateManager().getCurrent().onPlayerJoin(event);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        instance.getStateManager().getCurrent().onPlayerQuit(event);
        Bukkit.getScheduler().runTaskAsynchronously(instance, () -> instance.getSessionManager().unregister(event.getPlayer().getUniqueId()));
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        instance.getStateManager().getCurrent().onPlayerInteract(event);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        instance.getStateManager().getCurrent().onFoodLevelChange(event);
    }
}
